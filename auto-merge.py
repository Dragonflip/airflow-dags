import gitlab
import os

origin = "https://gitlab.com/Dragonflip/airflow-dags"
# Private token is set as an env var
gl = gitlab.Gitlab(origin, private_token, api_version='4')
gl.auth()

def create_merge_request(origin, private_token):
    mr = project.mergerequests.create({'source_branch': 'dev',
                               'target_branch': 'main',
                               'title': 'auto-merge from dev'})
    mr.assignee_id = gl.users.get(2).id # Assign it to coworker

def lookup_last_pipeline(origin, private_token):
    current_pipeline_id = os.environ['CI_PIPELINE_ID']
    pipelines = gl.projects.get(os.environ['CI_PROJECT_ID']).pipelines.list()
    for pipeline in pipelines:
        if pipeline.status == 'success' and pipeline.id == current_pipeline_id:
            create_merge_request()
