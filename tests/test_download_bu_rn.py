from pytest import mark

from dags.get_raw_data_bu import filter_links_rn


@mark.parametrize(
    'entrada',
    ['https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_AC_181120201549.zip', 'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_AC_181120201549.zip.sha512',
    'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_2t_AC_301120201245.zip', 'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_2t_AC_301120201245.zip.sha512',
    'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_AL_181120201549.zip', 'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_AL_181120201549.zip.sha512']
)
def test_links_sem_rn_devem_retornar_false(entrada):

    esperado = False
    resultado = filter_links_rn(entrada)
    assert resultado == esperado


@mark.parametrize(
    'entrada',
    ['https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_RN_181120201549.zip', 'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_2t_RN_301120201245.zip']
)
def test_links_com_rn_sem_sha_devem_retornar_true(entrada):

    esperado = True
    resultado = filter_links_rn(entrada)

    assert resultado == esperado



@mark.parametrize(
    'entrada',
    ['https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_1t_RN_181120201549.zip.sha512', 'https://cdn.tse.jus.br/eleicoes2020/buweb/bweb_2t_RN_301120201245.zip.sha512']
)
def test_links_com_rn_com_sha_devem_retornar_false(entrada):

    esperado = False
    resultado = filter_links_rn(entrada)

    assert resultado == esperado
