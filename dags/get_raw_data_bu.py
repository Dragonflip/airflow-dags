from datetime import datetime, timedelta
from airflow import DAG
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator

DW = PostgresHook(postgres_conn_id='dw', schema='nano')
BASE_URL = 'https://dadosabertos.tse.jus.br/dataset/resultados-2020-boletim-de-urna'
default_args = {
    'owner': 'default_user',
    'start_date': '2021-01-01',
    'retries': 5,
    'retry_delay': timedelta(minutes=1),
}

def _get_links(**context):
    import requests
    from bs4 import BeautifulSoup


    ti = context["ti"]
    response = requests.get(BASE_URL)

    soup = BeautifulSoup(response.text, 'html.parser')
    resource_section = soup.find("section", class_="resources")
    a_tags = resource_section.find_all("a", class_="resource-url-analytics")

    links = []
    for a in a_tags:
        links.append(a.attrs.get("href"))

    ti.xcom_push(key="available_links", value=links)


def filter_links_rn(link):
    filename = link.split("/")[-1].upper()
    return "RN" in filename and "SHA" not in filename


def _filter(**context):

    ti = context["ti"]
    available_links = ti.xcom_pull(task_ids="get_links_to_download_bu_rn", key="available_links")
    filtered_links = list(filter(filter_links_rn, available_links))
    ti.xcom_push(key="filtered_links", value=filtered_links)

def download_file(link):
    import requests
    import tempfile
    import glob
    from zipfile import ZipFile

    headers = {
    'authority': 'cdn.tse.jus.br',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language': 'en-US,en;q=0.9',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',    
    }
    file = requests.get(link, headers=headers)
    tempdir = tempfile.gettempdir()
    file_zip = tempdir + '/file.zip'
    with open(file_zip, 'wb') as f:
        f.write(file.content)

    with ZipFile(file_zip, 'r') as zip_object:
        zip_object.extractall(tempdir)

    csvs = glob.glob(tempdir + '/*.csv')
    for csv in csvs:
    	load_csv(csv)

def load_csv(csv):
    import pandas as pd
    from sqlalchemy import create_engine
    user = "airflow"
    password = "akin2010"
    host = "34.135.109.28"
    port = "5432"
    db = "bu"
    engine = create_engine(f"postgresql://{user}:{password}@{host}:{port}/{db}", echo=False)
    df = pd.read_csv(csv, encoding = "ISO-8859-1", sep=";")
    df.to_sql('bu', con=engine, if_exists='append')
    return None

def _load(**context):
    ti = context["ti"]
    filtered_links = ti.xcom_pull(task_ids="filter_links_to_download_bu_rn", key="filtered_links")

    for link in filtered_links:
        download_file(link)


dag = DAG('download_raw_bu', description='extract data from tse to postgres',default_args=default_args, schedule_interval=None)

get_links = PythonOperator(task_id='get_links_to_download_bu_rn', python_callable=_get_links, dag=dag)
filter_rn = PythonOperator(task_id='filter_links_to_download_bu_rn', python_callable=_filter, dag=dag)
load_links = PythonOperator(task_id='load_arquivos_to_dw', python_callable=_load, dag=dag)

get_links >> filter_rn >> load_links
